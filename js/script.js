function ImgSlide (queryClass, time) {
  const imgArr = document.querySelectorAll('.image-to-show');

    imgArr.forEach((elem) => {
        elem.style.display = 'none'
    });

    let stop = document.createElement('span');

    document.body.appendChild(stop);
    stop.textContent = 'Прекратить';
    stop.classList.add('stopBtn')

    let resume = document.createElement('span');

    document.body.appendChild(resume);
    resume.textContent = 'Возобновить показ';
    resume.classList.add('resumeBtn')

    let counter = 0;

    let flag = true;

    this.slider = () => {
        imgArr[counter].style.display = 'block';
        let slideMove = setTimeout(vanishSlide, time);
        stop.addEventListener('click', () => {
            clearTimeout(slideMove);
            flag = false;
        });

        resume.addEventListener('click', () => {
            if (flag === false) {
                slideMove = setTimeout(vanishSlide, time);
                flag = true;
            }
        });
    };

    let vanishSlide = () => {
        imgArr[counter].style.display = 'none';
        counter += 1;
        if (counter === imgArr.length) {
            counter = 0;
        }
        this.slider();
    };
}


let newSlide = new ImgSlide ('.image-to-show', 3000);

newSlide.slider();

