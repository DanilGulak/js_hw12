## Вопросы: 
1. Опишите своими словами разницу между функциями setTimeout() и setInterval().
2. Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
3. Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

**Ответ:**

1. setTimeout - предназначена для того чтобы вызвать функцию один раз через какой-то отрезок времени, setTimeout - регулярный вызов функции через определенные промежутки времени
2. Она не сработает мгновенно, потому что там есть задержка в 0.4s
3. Если не прописать clearInterval то цикл будет продолжаться